cmake_minimum_required(VERSION 3.12)
project(co)

set(CMAKE_CXX_STANDARD 20)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

include_directories(include)
include_directories(extern/googletest/googletest/include)
include_directories(extern/json/include)

add_subdirectory(source)
add_subdirectory(tests)
add_subdirectory(extern/googletest)